import { DataService } from './data.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class ConfigService {
  constructor(private _dataService: DataService) {}
  
  set setUrl(apiUrl: string){
    this._dataService._apiUrl = apiUrl;
  }
  
  getMenu(querystring: string): Observable<any[]> {
    return this._dataService.get(`menu/${querystring}`);
  }
}