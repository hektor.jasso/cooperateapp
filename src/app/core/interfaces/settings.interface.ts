export interface menuNode {
    id_Num_Opcion:number;
    id_Cnsc_Elem:number;
    nom_Opcion:string;
    desc_Opcion:string;
    nom_Objeto:string;
    path_Virtual:string;
    bit_Activo:boolean;
    usu_alta:number;
    id_Opcion_Padre:number;
    workflowDefinitionId:number;
    hijos?:menuNode[];
}