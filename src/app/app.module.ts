import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import  { createCustomElement } from '@angular/elements';
import { FormsModule } from '@angular/forms';
import  { Injector} from '@angular/core';



import { AppComponent } from './app.component';
//import { AppRoutingModule } from './app-routing.module';
import { MenuComponent } from './shared/menu/menu.component';
import { ContentComponent } from './shared/content/content.component';
import { MaterialModule } from './shared/material.module';
import { HttpClientModule } from '@angular/common/http';

import { ConfigService } from './core/services/config.service';
import { DataService } from './core/services/data.service';
import { AppSettings } from './core/app-settings';


@NgModule({
  imports:      [ 
    BrowserModule, 
    FormsModule,
    MaterialModule,
    HttpClientModule,
    //AppRoutingModule,
    BrowserAnimationsModule
  ],
  entryComponents :  [
    MenuComponent
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  providers: [AppSettings, DataService, ConfigService],
  declarations: [ AppComponent, MenuComponent, ContentComponent ],
  //bootstrap:    [ AppComponent ] //<-- enable when building app & disable to extract elements.
})
export class AppModule { 
  constructor(private injector : Injector){}
  ngDoBootstrap(){
    const el = createCustomElement(MenuComponent, {injector : this.injector});
    customElements.define('neisoft-menu',el);
  }
}