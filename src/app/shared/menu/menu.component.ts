import { ChangeDetectorRef, AfterContentChecked, Component, Input, ViewEncapsulation, OnChanges, SimpleChanges, AfterViewInit, ViewChild, ElementRef, ɵSWITCH_CHANGE_DETECTOR_REF_FACTORY__POST_R3__ } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { MediaMatcher } from '@angular/cdk/layout';
//Implements ability to expose data within tree objects.
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { MatToolbar } from '@angular/material/toolbar';
import { MatDrawer } from '@angular/material/sidenav';
import { NestedTreeControl } from '@angular/cdk/tree';

import { ConfigService }  from '../../core/services/config.service';
import * as node from '../../core/interfaces/settings.interface';
import { AppSettings  } from '../../core/app-settings';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MenuComponent implements OnChanges, AfterViewInit, AfterContentChecked {
  @ViewChild('divContent') divContent: ElementRef;
  @ViewChild('divEl') divEl: ElementRef;
  @ViewChild('index') divIndex: ElementRef;
  @ViewChild('drawer') drawer: MatDrawer;
  @ViewChild('toolbar') toolbar:MatToolbar;

  @Input() querystring: string = null;
  @Input() urlapi: string = null;
  @Input() appname: string = 'Neitek';
  @Input() host: string = null;
  @Input() footercode: string = 'primary';
  @Input() bodycode: string = '#f6f9fa';

  mobileQuery: MediaQueryList;
  treeControl = new NestedTreeControl<node.menuNode>(node => node.hijos);
  dataSource = new MatTreeNestedDataSource<node.menuNode>();

  private _mobileQueryListener: () => void;

  constructor(private changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, private appSettings: AppSettings, private configService: ConfigService) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngAfterViewInit():void {
    //this.pageSize(this.divIndex.nativeElement, true);
    switch(this.footercode){
      case 'primary':
        this.toolbar.color = "primary";
      break;
      case 'accent':
        this.toolbar.color = "accent";
      break;
      case 'warn':
        this.toolbar.color = "warn";
      break;
      default:
        this.toolbar.color = "primary";
      break;
    }
  }

  ngAfterContentChecked():void {
    this.changeDetectorRef.detectChanges();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(this.urlapi){
      if(changes['urlapi'].previousValue != changes['urlapi'].currentValue){
        this.loadMenu();
      }
    }
  }

  onNavigate(url:string): void {
    this.appSettings.setUrl = (this.host + url);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  pageSize(div: HTMLDivElement, isExpanded: boolean):void {
    let element: HTMLDivElement = this.divContent.nativeElement;
    let drawer = (this.drawer as any)._elementRef.nativeElement;
    if(isExpanded){
      div.style.height = (div.clientHeight + 200).toString() + 'px';
      element.style.height = (div.clientHeight + 200).toString() + 'px';
      drawer.style.height = (div.clientHeight + 200).toString() + 'px';
    }
    else{
      div.style.height = (div.clientHeight - 200).toString() + 'px';
      element.style.height = (div.clientHeight - 200).toString() + 'px';
      drawer.style.height = (div.clientHeight - 200).toString() + 'px';
    }
    
  }

  loadMenu(): void {
    this.configService.setUrl = this.urlapi;
    /*//<-- testing purposes, comment before deploy.
    this.dataSource.data = this.appSettings.collection;
    setTimeout(()=>{
      this.pageSize(this.divEl.nativeElement, true);
    }, 1000);
    */
    
    //enable to properly run the app logic. 
    this.configService.getMenu(this.querystring).subscribe((data:any[])=>{
      if(data){
        if(data instanceof HttpErrorResponse){
          console.log("Error: " + (data as any).message);
        }
        else {
          this.dataSource.data = data;
          this.pageSize(this.divEl.nativeElement, true);
          this.drawer.open();
        }
      }
    });
  }

  hasChild = (_: number, node: node.menuNode) => !!node.hijos && node.hijos.length > 0;
  iconName = (option: string): string => {
    let icon:string ='';
    switch(option){
      case 'Recurso':icon = 'settings';
        break;
      case 'Forma':icon ='category';
        break;
      case 'Flujo':icon ='compare_arrows';
        break;
      case 'Reporte':icon ='content_paste';
        break;
      case 'Informe':icon ='event_note';
        break;
      case 'Documento':icon ='description';
        break;
      case 'Compuesta':icon ='dashboard';
        break;
      case 'InfoBI':icon ='api';
        break;
      case 'Repositorio':icon ='dns';
        break;
      case 'Tarea':icon ='work';
        break;
      case 'Relacional':icon ='device_hub';
        break;
      default:icon = 'folder';
        break;
    }
    return icon;
  }
}
