import { Component, ViewChild, ElementRef, Input, AfterViewInit  } from '@angular/core';
import { AppSettings  } from '../../core/app-settings';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html'
})
export class ContentComponent implements AfterViewInit {
  @Input() isOpened: Boolean;
  @Input() elWidth: number;
  @Input() maxWidth: number;
  @Input() color: string = '#f6f9fa';
  @ViewChild('content') content: ElementRef;

  constructor(private appSettings: AppSettings) {
    this.appSettings.navigateToUrl.subscribe((newUrl:string)=>{
      if(newUrl){
        this.loadContent(newUrl);
      }
    });
  }

  ngAfterViewInit():void{
    let iframe: HTMLIFrameElement = this.content.nativeElement;
    iframe.style.background = this.color;
  }

  loadContent(url:string): void {
    let iframe: HTMLIFrameElement = this.content.nativeElement;
    iframe.src =  url;
    iframe.focus();
  }
}
