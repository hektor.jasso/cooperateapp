const fs = require('fs-extra');
const concat = require('concat');
(async function build() {
const files = [
'./dist/neisoft/runtime-es5.js',
'./dist/neisoft/polyfills-es5.js',
'./dist/neisoft/main-es5.js',
]

const files2015 = [
    './dist/neisoft/runtime-es2015.js',
    './dist/neisoft/polyfills-es2015.js',
    './dist/neisoft/main-es2015.js',
]
await fs.ensureDir('./dist/elements')

await concat(files, './dist/elements/neisoft-app.js');
await concat(files2015, './dist/elements/neisoft-app.es2015.js');

await fs.copyFile('./dist/neisoft/styles.css', './dist/elements/styles.css')
//await fs.copy('./dist/neisoft/assets/', 'elements/assets/' )
})()